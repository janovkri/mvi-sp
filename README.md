# Pattern Recognition on Wafer Bin Maps
### Zadání:
Úkolem je navrhnout CNN klasifikátor pro detekci defektů na wafer mapách. Práce využívá Keras/Tensorflow a spočívá v sestrojení dvou CNN, 
předtréninku první z nich na datasetu WM-811K a následném tréninku druhé sítě na datasetu firmy OC Semiconductor s využitím extrakce příznaků a k-násobné křížové validace.

### Odkazy:
- kód k práci naleznete také zde: https://colab.research.google.com/drive/1MIna-tZGx3rjovf_SuVLrNYKmSY3oB2B?usp=sharing
- dataset WM-811K naleznete zde: https://www.kaggle.com/qingyi/wm811k-wafer-map
- kód verze práce s přidanými precision, recall, f1-score: https://colab.research.google.com/drive/1VLSyxipm2rvE4_DZJVw8SUk-mTheo0bo?usp=sharing
